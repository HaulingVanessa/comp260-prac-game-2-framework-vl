﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	private Rigidbody rb;
	public Transform startingPos;

	public float speed = 5f;
	public float force = 5f;

	public int player;
		
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void ResetPosition() {
		//teleport to the starting position
		rb.MovePosition(startingPos.position);
		//stop it from moving
		rb.velocity = Vector3.zero;
	}

	void FixedUpdate(){
			float moveHorizontal = Input.GetAxis ("Horizontal");
			float moveVertical = Input.GetAxis ("Vertical");

			Vector3 movement = new Vector3 (moveHorizontal, 0, moveVertical);

			rb.velocity = movement.normalized * speed;
		//Rigidbody is the physics device, transform is the objects physical location(including rotation and size)
	}
}

